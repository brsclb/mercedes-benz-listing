import React from 'react';
import {Route, Routes} from "react-router-dom";
import CarsListing from "./pages/CarsListing";
import CarEdit from "./pages/CarEdit";
import DefaultLayout from "./components/layouts/DefaultLayout";
import {StockInfoProvider} from "./lib/contexts/StockContext";

function App() {
  return (
    <Routes>
      <Route element={<DefaultLayout />}>
        <Route index element={<StockInfoProvider><CarsListing /></StockInfoProvider>} />
        <Route path="/:id" element={<CarEdit />} />
      </Route>
    </Routes>
  );
}

export default App;
