export interface StockInfo {
  id: number
  carId: string
  inStock: boolean
  horsePower: number
  price: string
  color: string
}
