import axios from "axios";
import { BASE_URL } from "./urlCollection";

const instance = axios.create({
  baseURL: BASE_URL,
});

// Intercepts every http request and we can pass the custom config data here.
instance.interceptors.request.use((config) => {
  const anyKey: string | null = null;
  if (anyKey) {
    config.headers = Object.assign({
      "ANY-HEADER": anyKey
    }, config.headers);
  }

  return config;
});

export default instance;
