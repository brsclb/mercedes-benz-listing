import axios from "./interceptor";
const api = `${process.env.REACT_APP_API_ADDRESS}`;

const services = {
  async get(route: string, config: any = {}) {
    return await axios.get(`${api}/${route}`, config);
  },

  async post(route: string, data: any) {
    return await axios.post(`${api}/${route}`, data);
  },

  async put(route: string, data: any) {
    return await axios.put(`${api}/${route}`, data);
  },

  async patch(route: string, data: any) {
    return await axios.patch(`${api}/${route}`, data);
  },

  async delete(route: string) {
    return await axios.delete(`${api}/${route}`);
  }
}

export default services;
