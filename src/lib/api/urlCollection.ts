export const BASE_URL = `${process.env.REACT_APP_API_ADDRESS}`;

// CARS
export const CARS_URL = 'cars';

// STOCK INFO
export const STOCKS_URL = 'stocks';

// COLORS
export const COLORS_URL = "colors";
