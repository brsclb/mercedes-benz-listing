import api from './general.service';
import * as url from './urlCollection';
import {StockInfo} from "../schemas/Cars";

export const getStockInfo = async () => {
  return await api.get(url.STOCKS_URL);
}

export const getColors = async () => {
  return await api.get(url.COLORS_URL);
}

export const updateStockData = async (data: StockInfo) => {
  return await api.patch(`${url.STOCKS_URL}/${data.id}`, data);
}
