import {StockInfo} from "../schemas/Cars";
import {createContext, ReactNode, useCallback, useReducer, useState} from "react";
import {getStockInfo, getColors, updateStockData} from "../api/services";

// CONTEXT
interface StockInfoContextProps {
  loading: boolean
  actionLoading: boolean
  stockData: StockInfo[]
  colors: string[]
  getColors: () => void
  getStockData: () => void
  editStockData: (data: StockInfo) => Promise<boolean>
}

const StockInfoContext = createContext<StockInfoContextProps>({
  loading: true,
  actionLoading: true,
  stockData: [],
  colors: [],
  getColors: () => {},
  getStockData: () => {},
  editStockData: (data: StockInfo) => {return new Promise((resolve, reject): boolean => {return true;})}
});

// STORE
interface State {
  stockData: StockInfo[],
  colors: string[]
}

const INITIAL_STATE: State = {
  stockData: [],
  colors: []
}

enum Kind {
  INITIALIZE_STOCKS,
  INITIALIZE_COLORS,
  EDIT
}

interface InitializeStocksAction {
  kind: Kind.INITIALIZE_STOCKS
  stockData: StockInfo[]
}

interface InitializeColorsAction {
  kind: Kind.INITIALIZE_COLORS
  colors: string[]
}

interface EditAction {
  kind: Kind.EDIT
  stockObject: StockInfo
}

type Action = InitializeStocksAction | InitializeColorsAction | EditAction;

const reducer = (state: State, action: Action): State => {
  switch (action.kind) {
    case Kind.INITIALIZE_STOCKS:
      return {...state, stockData: action.stockData}

    case Kind.INITIALIZE_COLORS:
      return {...state, colors: action.colors}

    case Kind.EDIT:
      let tmpAllStockInfo = [...state.stockData];
      let updatedIndex = tmpAllStockInfo.findIndex(el => el.id === action.stockObject.id);
      if (updatedIndex >= 0) {
        tmpAllStockInfo[updatedIndex] = action.stockObject;
        return {...state, stockData: tmpAllStockInfo}
      } else {
        return state;
      }
    default:
      return state;
  }
}

// PROVIDER
export const StockInfoProvider = (props: { children: ReactNode }) => {

  const [loading, setLoading] = useState<boolean>(true);
  const [actionLoading, setActionLoading] = useState<boolean>(false);
  const [state, dispatch] = useReducer(reducer, INITIAL_STATE);

  const getStocksData = async () => {
    try {
      setLoading(true);
      const response = await getStockInfo();

      if (response && [200, 201].includes(response.status)) {
        dispatch({kind: Kind.INITIALIZE_STOCKS, stockData: response.data ?? []});
        setLoading(false);
      } else {
        dispatch({kind: Kind.INITIALIZE_STOCKS, stockData: []});
        setLoading(false);
      }

    } catch (error) {
      console.error(error);
    }
  }

  const getColorsData = async () => {
    try {
      setLoading(true);
      const response = await getColors();

      if (response && [200, 201].includes(response.status)) {
        dispatch({kind: Kind.INITIALIZE_COLORS, colors: response.data ?? []});
        setLoading(false);
      } else {
        dispatch({kind: Kind.INITIALIZE_COLORS, colors: []});
        setLoading(false);
      }

    } catch (error) {
      console.error(error);
    }
  }

  const editStockData = useCallback(async (data: StockInfo): Promise<boolean> => {
    setActionLoading(true);
    try {
      const response = await updateStockData(data);
      if (response && [200, 201].includes(response.status)) {
        dispatch({kind: Kind.EDIT, stockObject: response.data});
        setActionLoading(false);
        return true;
      } else {
        setActionLoading(false);
        return false;
      }
    } catch (error) {
      console.error(error);
      setActionLoading(false);
      return false;
    }
  }, []);

  return (
    <StockInfoContext.Provider
      value={
        {
          stockData: state.stockData,
          colors: state.colors,
          loading: loading,
          actionLoading: actionLoading,
          getColors: getColorsData,
          getStockData: getStocksData,
          editStockData: editStockData,
        }
      }
    >
      {props.children}
    </StockInfoContext.Provider>
  )
}

export default StockInfoContext;
