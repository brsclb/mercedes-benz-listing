import {useContext} from "react";
import StockInfoContext from "../contexts/StockContext";

const useStockInfo = () => {
  return useContext(StockInfoContext);
}

export default useStockInfo;
