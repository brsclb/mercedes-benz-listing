import {Card, Row, Col, CardBody} from "reactstrap";
import CarsTable from "../components/cars/CarsTable";
import useStockInfo from "../lib/hooks/useStockInfo";
import {useEffect} from "react";
import Loading from "../components/common/Loading";

const CarsListing = () => {
  const stockInfoCtx = useStockInfo();

  useEffect(() => {
    stockInfoCtx.getColors();
    stockInfoCtx.getStockData();
  }, []);

  return (
    <Row>
      <Col xs="12">
        <Card className="border-0">
          <CardBody>
            {
              stockInfoCtx.loading
                ?
                <Loading />
                :
                stockInfoCtx.stockData.length > 0
                  ?
                  <CarsTable/>
                  :
                  <p className="text-muted fs-4 text-center">No stock information.</p>
            }
          </CardBody>
        </Card>
      </Col>
    </Row>
  )
}

export default CarsListing;
