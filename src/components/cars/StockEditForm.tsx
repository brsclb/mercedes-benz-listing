import {useFormik} from "formik";
import * as Yup from "yup";
import {StockInfo} from "../../lib/schemas/Cars";
import useStockInfo from "../../lib/hooks/useStockInfo";
import {Form, FormFeedback, Input, Label} from "reactstrap";
import {useMemo} from "react";

interface StockEditFormProps {
  editStockData: StockInfo
  cancel(): void
}

const StockEditForm = (props: StockEditFormProps) => {
  const stockInfoCtx = useStockInfo();

  // Form validation
  const validation = useFormik({
    enableReinitialize: true,
    initialValues: props.editStockData,
    validationSchema: Yup.object({
      inStock: Yup.boolean(),
      horsePower: Yup.number().min(100).max(550).required('Please enter horse power between 100-550'),
      price: Yup.number().required("Please enter price."),
      color: Yup.string()
    }),
    onSubmit: async (data: StockInfo) => {
      try {
        const response = await stockInfoCtx.editStockData(data);
        if (response) {
          props.cancel();
        }
      } catch (error) {
        console.log(error);
      }
    }
  });

  const hasChanges = useMemo(() => {
    return JSON.stringify(validation.values) !== JSON.stringify(props.editStockData)
  }, [validation.values]);

  return (
    <div className="p-2">
      <Form
        className="form-horizontal"
        onSubmit={(e) => {
          e.preventDefault();
          validation.handleSubmit();
          return false;
        }}
      >
        <p className="mb-1"><b>Stock ID:</b> {props.editStockData.id}</p>
        <p><b>Car ID:</b> {props.editStockData.carId}</p>

        <div className="mb-4">
          <div className="form-check form-check-right mb-3">
            <Input
              name="inStock"
              className="form-check-input border-none"
              type="checkbox"
              onChange={validation.handleChange}
              onBlur={validation.handleBlur}
              value=""
              id="inStockCheck"
              defaultChecked={validation.values.inStock}
            />
            <Label
              htmlFor="inStockCheck"
              className="form-check-label fw-bold"
            >
              In Stock
            </Label>
          </div>
        </div>

        <div className="mb-4">
          <Label className="form-label fw-bold">Horse Power</Label>
          <Input
            name="horsePower"
            className="form-control border-none"
            placeholder="Title"
            type="range"
            min={100}
            max={550}
            onChange={validation.handleChange}
            onBlur={validation.handleBlur}
            value={validation.values.horsePower || ""}
            invalid={!!(validation.touched.horsePower && validation.errors.horsePower)}
          />
          <div className="d-flex justify-content-between align-items-center mt-1">
            <span className="text-muted fw-light fs-6">100</span>
            <span className="fw-bold fs-5">{validation.values.horsePower}</span>
            <span className="text-muted fw-light fs-6">550</span>
          </div>
          {validation.touched.horsePower && validation.errors.horsePower ? (
            <FormFeedback type="invalid">{validation.errors.horsePower}</FormFeedback>
          ) : null}
        </div>

        <div className="mb-4">
          <Label className="form-label fw-bold">Price</Label>
          <Input
            name="price"
            className="form-control"
            placeholder="Price"
            type="number"
            onChange={validation.handleChange}
            onBlur={validation.handleBlur}
            value={validation.values.price || ""}
            invalid={!!(validation.touched.price && validation.errors.price)}
          />
          {validation.touched.price && validation.errors.price ? (
            <FormFeedback type="invalid">{validation.errors.price}</FormFeedback>
          ) : null}
        </div>


        <div className="mb-4">
          <h5 className="fs-6 fw-bold mb-4">Color</h5>

          <div className="d-flex align-items-center gap-3 flex-wrap">
            {
              stockInfoCtx.colors.map(el =>
                <div key={`color-key-${el}`}>
                  <div className="form-check form-check-right mb-3">
                    <Input
                      name="color"
                      className="form-check-input"
                      placeholder="Color"
                      type="radio"
                      id={`color-${el}`}
                      onChange={validation.handleChange}
                      onBlur={validation.handleBlur}
                      value={el}
                      defaultChecked={el === validation.values.color}
                    />
                    <Label
                      className="form-check-label text-capitalize"
                      htmlFor={`color-${el}`}
                    >
                      { el }
                    </Label>
                  </div>
                </div>
              )
            }
          </div>
        </div>

        <div className="mt-4 d-flex align-items-center gap-3">{
          props.cancel &&
          <button
            className="btn btn-secondary btn-block px-4 flex-fill"
            type="button"
            onClick={props.cancel}
            disabled={stockInfoCtx.actionLoading}
          >
            Cancel
          </button>
        }
          <button
            className="btn btn-primary btn-block btn-block px-4 flex-fill"
            type="submit"
            disabled={stockInfoCtx.actionLoading || !hasChanges}
          >
            Save
          </button>
        </div>
      </Form>
    </div>

  )
}

export default StockEditForm;
