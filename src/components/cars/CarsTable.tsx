import {Table} from "reactstrap";
import useStockInfo from "../../lib/hooks/useStockInfo";
import ModalAction from "../common/ModalAction";
import {useState} from "react";
import StockEditForm from "./StockEditForm";
import {StockInfo} from "../../lib/schemas/Cars";

const CarsTable = () => {
  const stockInfoCtx = useStockInfo();
  const [showEditForm, setShowEditForm] = useState<boolean>(false);
  const [selectedStockInfo, setSelectedStockInfo] = useState<StockInfo | null>(null);

  const selectToEdit = (item: StockInfo) => {
    setSelectedStockInfo(item);
    setShowEditForm(true);
  }

  const cancelEdit = () => {
    setShowEditForm(false);
    setSelectedStockInfo(null);
  }

  return (
    <div className="table-responsive">
      <Table className="table table-striped mb-0 items-centered">
        <thead>
        <tr>
          <th>ID</th>
          <th>Car ID</th>
          <th>In Stock</th>
          <th>HP</th>
          <th>Price</th>
          <th>Color</th>
          <th>Actions</th>
        </tr>
        </thead>
        <tbody>
        {
          stockInfoCtx.stockData.map(el =>
            <tr key={el.id}>
              <th scope="row">{el.id}</th>
              <td>{ el.carId }</td>
              <td>{ el.inStock ? 'True' : 'False'}</td>
              <td>{ el.horsePower }</td>
              <td>{ el.price } €</td>
              <td><div className="text-capitalize d-flex align-items-center gap-2"><div className="color-box" style={{background: el.color}}></div> { el.color }</div></td>
              <td><button className="btn btn-primary" onClick={() => selectToEdit(el)}>Edit</button></td>
            </tr>
          )
        }
        </tbody>
      </Table>

      <ModalAction
        title="Edit Stock"
        cancel={() => setShowEditForm(false)}
        show={showEditForm}
        content={
          selectedStockInfo && <StockEditForm cancel={() => cancelEdit()} editStockData={selectedStockInfo}/>
        }
      />
    </div>
  )
}

export default CarsTable;
