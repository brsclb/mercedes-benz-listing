import React from "react";
import {Modal, ModalBody, ModalFooter} from "reactstrap";

interface ModalProps {
  show: boolean
  content?: React.ReactNode;
  title?: string
  actionText?: string
  cancelText?: string
  cancel?(): void
  actionButton?(): void
  backdrop?: 'static' | undefined
}

const ModalAction = (props: ModalProps) => {

  return (
    <Modal
      isOpen={props.show}
      toggle={props.cancel}
      centered={true}
      backdrop={props.backdrop ? props.backdrop : undefined}
    >
      <div className="modal-header">
        <h5 className="modal-title mt-0" id="modal-title">
          { props.title }
        </h5>
        <button
          type="button"
          onClick={props.cancel}
          className="close"
          data-dismiss="modal"
          aria-label="Close"
        >
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <ModalBody>
        { props.content }
      </ModalBody>

      {
        (props.cancelText || props.actionText) &&

        <ModalFooter>
          { props.cancelText &&
            <button
              className="btn btn-secondary btn-block px-4"
              type="button"
              onClick={props.cancel}
            >
              {props.cancelText || 'Cancel'}
            </button>
          }

          { props.actionText &&
            <button
              className="btn btn-primary btn-block px-4"
              type="button"
              onClick={props.actionButton}
            >
              {props.actionText || 'Save'}
            </button>
          }
        </ModalFooter>
      }

    </Modal>
  )
}

export default ModalAction;
