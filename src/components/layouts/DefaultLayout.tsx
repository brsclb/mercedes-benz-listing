import {Outlet} from "react-router-dom";

const DefaultLayout = () => {
  return (
    <div className="page-content">
      <div className="container">
        <Outlet/>
      </div>
    </div>
  )
}

export default DefaultLayout;
