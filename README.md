# Mercedes-Benz Car Listing

In this project React, Typescript, Bootstrap, json-server are used.

## Available Scripts

Before you run anything after cloning, please run

### `npm install`

After completed, in the project directory, you can run:

### `json-server --watch fake-db.json --port 3001`

or 

### `npx json-server --watch fake-db.json --port 3001`

to enable the fake database. Without running this, you won't be able to see any data. 
After running this you can run the project itself.

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

